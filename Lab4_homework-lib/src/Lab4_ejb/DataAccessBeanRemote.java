/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4_ejb;


import javax.ejb.Remote;

/**
 *
 * @author student
 */
@Remote
public interface DataAccessBeanRemote {

    String getRandomNumbersTable();
    void setRandomNumbersTable();
}
